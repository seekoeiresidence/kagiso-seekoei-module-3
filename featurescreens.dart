import 'package:flutter/material.dart';
import 'package:mtn/main.dart';

void main2() {
  runApp(const MaterialApp(
    title: 'Login',

    home: ArticlesRoute(),
  ));
}

class ArticlesRoute extends StatelessWidget {
  const ArticlesRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Articles feed'),
      ),
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.start,
         children: [ 
           const SizedBox(height: 20),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                    ListTile(
                    leading: Icon(Icons.check_circle, color: Colors.blue),
                    title: Text('J.K Rowling', style: TextStyle(fontWeight: FontWeight.bold),),
                    subtitle: Text('The day the earth stood still. For time and time again there where..', style: TextStyle(color: Colors.black),),
                  ),
                ],
              ),
           ),
           const SizedBox(height: 10),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    ListTile(
                    leading: const Icon(Icons.check_circle, color: Colors.blue),
                    title: const Text('Dr. Stephen Hawking', style: TextStyle(fontWeight: FontWeight.bold),),
                    subtitle: const Text('The secrets of Quantum Mechanics can never be fathomed by human intellect alone. AI will play a...', style: TextStyle(color: Colors.black),),
                    onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => const SecondRoute()),);
                    },
                  ),

                ],
              ),
           ),
           const SizedBox(height: 10),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    ListTile(
                    leading: const Icon(Icons.check_circle, color: Colors.blue),
                    title: const Text('Oprah Winfrey', style: TextStyle(fontWeight: FontWeight.bold),),
                    subtitle: const Text('Dealing with anxiety and stress: Meditation and psycho-therapy could be the answer. A new study has found..', style: TextStyle(color: Colors.black),),
                    onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => const SecondRoute()),);
                    },
                  ),

                ],
              ),
           ),
         ],                                                                   
       )

    );
  }
}





class InboxRoute extends StatelessWidget {
  const InboxRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inbox'),
      ),
         body: Column(
         crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.start,
         children: const [ 
          SizedBox(height: 20),
           Text('No new messages', style: TextStyle(color: Colors.grey, fontSize: 15),)
         ],                                                                   
       )

    );
  }
}