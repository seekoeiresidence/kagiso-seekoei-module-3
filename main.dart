import 'package:flutter/material.dart';
import 'package:mtn/dashboard.dart';
import 'package:mtn/featurescreens.dart';

void main() {
  runApp(const MaterialApp(
    title: 'Login',

    home: FirstRoute(),
  ));
}

class FirstRoute extends StatelessWidget {
  const FirstRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.center,
         children: [ 
           Container(        
             height: 100,
             width: 100,
             child: Image.asset('assets/a.png'),
           ),

           const SizedBox(height: 30),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter username',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter Password',
           ),
           ),
           ),
           const SizedBox(height: 50),
           ElevatedButton(
              style: style,
                     onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const DashboardRoute()),);
                     },
                     child: const Text('login'),
           ),

           const SizedBox(height: 30),
           
           TextButton(
             style: TextButton.styleFrom(
               textStyle: const TextStyle(fontSize: 20, color: Colors.blue),
             ),
                onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const SecondRoute()),);
                },
             child: const Text('New here? Register'),
           ),
           
          

         ],                                                                   
         


       )

    );
  }
}

class SecondRoute extends StatelessWidget {
  const SecondRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
      ),

/********************* */
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.center,
         children: [ 
           Row(children: [Text('Register',style: TextStyle(fontSize: 40, color: Colors.blue,),)],),

           const SizedBox(height: 30),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter username',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter email',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter password',
           ),
           ),
           ),           
           const SizedBox(height: 50),
           ElevatedButton(
              style: style,
                     onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const FirstRoute()),);
                     },
                     child: const Text('Register'),
           ),

           const SizedBox(height: 30),
           
           TextButton(
             style: TextButton.styleFrom(
               textStyle: const TextStyle(fontSize: 20, color: Colors.blue),
             ),
                onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const FirstRoute()),);
                },
             child: const Text('Already registered? Login'),
           ),
           
          

         ],                                                                   
         


       )

/********************* */

      );
  }
}




